<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel <br /> praca rekrutacyjna (Produkty)
                </div>

                <div class="links">
                    <div><a href="{{route('product.index')}}" style="color: red; font-size: 36px;">Przejdź do działu z produktami</a></div>
                    <br /><br /> Informacje:<br /><br />

                    Wymagane php>=5.6.4<br />
                    Wersja framework 5.4.*<br /><br />

                    1. Aby uruchomić należy skonfigurować bazę danych mysql w pliku .env<br />
                    2. Uruchomić migracje<br />
                    php artisan migrate<br />
                    3. Uruchomić seeder'y - skorzystałem z fakera aby zapełnić dane<br />
                    php artisan db:seed<br /><br />

                    Zadania<br />
                    1. Zrobione.<br />
                    2. Struktura bazy zgodna z przygotowanymi migracjami<br />
                    3. Widok pod routingiem /product<br />
                    4. Dodawanie produktów pod routingiem /product/create<br />
                    5. Usuwanie produktów możliwe z listy produktów /product<br /><br />

                    Komentarz i plany na przyszłość - by to co zostało przygotowane było lepsze:<br />
                    1. Zamiast przechowywania VAT'u jaku liczby, można pokusić się o relacjię do tabeli z VAT'ami<br />
                    2. Dodanie walidatora z dannyvankooten/laravel-vat<br />
                    3. W przyszłości można pokusić się o zastosowanie voyager<br />
                    https://github.com/the-control-group/voyager<br />
                    4. W przypadku walidacji VAT'u warto dać możliwość zmiany kierunku przeliczania z netto na brutto, z brutto na netto<br />
                    5. Utworzenie typów "ceny" np. cena hurtowa, cena minimalna itp tak by użytkownik mógł skonfigurować sobie takie typy. Wiążę to za sobą konieczność utworzenia Modelu PriceType, w którym byłaby możliwość dodania typów<br />
                    6. Przechowywanie cen archiwalnych.<br />
                </div>
            </div>
        </div>
    </body>
</html>
