<script id="hidden-template" type="text/x-custom-template">
    <div class="col-lg-12 price-element">
        <div class="form-group js-new-supplement">
            {!! Form::label('netto', 'Cena netto', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::number('netto{index}', null, array('class' => 'form-control netto', 'name' => 'prices[{index}][netto]', 'step' => '0.01' )) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('vat', 'Stawka VAT', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::number('vat{index}', null, array('class' => 'form-control vat', 'name' => 'prices[{index}][vat]', 'step' => '0.01')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('brutto', 'Cena brutto', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::number('brutto{index}', null, array('class' => 'form-control brutto', 'name' => 'prices[{index}][brutto]', 'step' => '0.01')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nazwa', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('name{index}', null, array('class' => 'form-control', 'name' => 'prices[{index}][name]')) !!}
            </div>
        </div>
        <div class="col-sm-10">
            <a class='btn btn-xs btn-default remove_price' href="#">Usuń</a>
        </div>
    </div>
</script>

@section('bottomscripts')
    <script>
        var remove_price = function () {
            $('.remove_price').click(function() {
                $(this).parents('div')[1].remove();
                //dbamy o indeksy
                var count = 0;
                $('.price-element').each(function (index) {
                    $(this).find('[name*="prices"]').each(function () {
                        var value = $(this).attr('name').replace(/\d+/, count);
                        $(this).attr('name', value);
                    });
                    count++;
                });
            });
        };

        var changePrice = function () {
            $('.form-control.brutto').on('input',function(e){
                var vat = $('.form-control.vat', $(this).parent().parent().parent()).val();
                var brutto = $('.form-control.brutto', $(this).parent().parent().parent()).val();
                var netto = $('.form-control.netto', $(this).parent().parent().parent());

                if ($.isNumeric(vat) && $.isNumeric(brutto)) {
                    var value = parseFloat(brutto) / (100.0 + parseFloat(vat)) * 100.0;
                    netto.val(value.toFixed(2));
                }
            });
            $('.form-control.netto').on('input',function(e){
                var vat = $('.form-control.vat', $(this).parent().parent().parent()).val();
                var brutto = $('.form-control.brutto', $(this).parent().parent().parent());
                var netto = $('.form-control.netto', $(this).parent().parent().parent()).val();

                if ($.isNumeric(vat) && $.isNumeric(netto)) {
                    var value = parseFloat(netto) * (100.0 + parseFloat(vat)) / 100.0;
                    brutto.val(value.toFixed(2));
                }
            });
            $('.form-control.vat').on('input',function(e){
                var vat = $('.form-control.vat', $(this).parent().parent().parent()).val();
                var brutto = $('.form-control.brutto', $(this).parent().parent().parent());
                var netto = $('.form-control.netto', $(this).parent().parent().parent()).val();

                if ($.isNumeric(vat) && $.isNumeric(netto)) {
                    var value = parseFloat(netto) + (parseFloat(netto) * parseFloat(vat) / 100.0);
                    brutto.val(value.toFixed(2));
                }
            });
        };

        $(document).ready(function() {
            // functionality for the 'Add one' button
            var template = $('#hidden-template').html();
            $('#add_price').click(function() {
                if( $("#no_price").length) {
                    $("#no_price").remove();
                }
                var index = $('.js-new-supplement').length;
                $('#prices').append(template.replace(/\{index\}/g, index));
                remove_price();
                changePrice();
            });
            remove_price();
            changePrice();
        });
    </script>
@endsection