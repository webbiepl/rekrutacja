@extends('layout')

@section('title', 'Dodawanie produktu')

@section('content')
    <h1>Dodawanie produktu</h1>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route' => 'product.store']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nazwa produktu') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Opis produktu') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

        <h3>Ceny: <a class='btn btn-xs btn-default' href="#" id="add_price">Dodaj</a></h3>

        <div id="no_price">Brak ceny</div>
        <div id="prices"></div>
        <hr>

        @include('product.price_template')

        <div class="form-group form-submit-group">
            {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
            {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-default']) !!}
        </div>

    {!! Form::close() !!}
@endsection