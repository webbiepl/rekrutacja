@extends('layout')

@section('title', 'Lista produktów')

@section('content')
    <h1>Lista produktów</h1>
    <a href="{{route('product.create')}}" class="btn btn-primary">Dodaj produkt</a>


    <table class="table table-hover">
        <tr>
            <td>ID</td>
            <td>NAZWA</td>
            <td>CENY</td>
            <td>EDYTUJ</td>
            <td>USUN</td>
        </tr>
        @foreach($products as $product)
            <tr>
                <td>
                    {{ $product->id }}
                </td>
                <td>
                    {{ $product->name }}
                </td>
                <td>
                    @foreach($product->prices as $price)
                        {{ $price->name }} - {{ number_format($price->netto, 2, ',', '.') }} PLN - {{ number_format($price->brutto, 2, ',', '.') }} PLN<br />
                    @endforeach
                </td>
                <td>
                    <a class="btn btn-info" href="{{route('product.edit', $product)}}">Edytuj</a>
                </td>
                <td>
                    {!! Form::model($product, ['route' => ['product.destroy', $product], 'method' => 'delete']) !!}
                    <button class="btn btn-danger">Usuń</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
    {{ $products->links() }}
@endsection