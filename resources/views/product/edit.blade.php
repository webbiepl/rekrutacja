@extends('layout')

@section('title', 'Edycja produktu')

@section('content')
    <h1>Dodawanie produktu</h1>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($product, ['route' => ['product.update', $product], 'method' => 'PUT']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Nazwa produktu') !!}
        {!! Form::text('name', $product->name, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Opis produktu') !!}
        {!! Form::textarea('description', $product->description, ['class' => 'form-control']) !!}
    </div>

    <h3>Ceny: <a class='btn btn-xs btn-default' href="#" id="add_price">Dodaj</a></h3>

    <div id="prices">
        @foreach($product->prices as $key => $price)
            <div class="col-lg-12 price-element">
                <div class="form-group js-new-supplement">
                    {!! Form::label('netto', 'Cena netto', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::number('netto' . $key . '', $price->netto, array('step' => '0.01', 'class' => 'form-control netto', 'name' => 'prices[' . $key . '][netto]' )) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('brutto', 'Cena brutto', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::number('brutto' . $key . '', $price->brutto, array('step' => '0.01', 'class' => 'form-control brutto', 'name' => 'prices[' . $key . '][brutto]')) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('vat', 'Stawka VAT', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::number('vat' . $key . '', $price->vat, array('step' => '0.01', 'class' => 'form-control vat', 'name' => 'prices[' . $key . '][vat]')) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Nazwa', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('name' . $key . '', $price->name, array('step' => '0.01', 'class' => 'form-control', 'name' => 'prices[' . $key . '][name]')) !!}
                    </div>
                </div>
                <div class="col-sm-10">
                    <a class='btn btn-xs btn-default remove_price' href="#">Usuń</a>
                </div>
                {!! Form::hidden('id' . $key . '', $price->id, array('name' => 'prices[' . $key . '][id]')) !!}
            </div>
        @endforeach
    </div>
    <hr>

    @include('product.price_template')

    <div class="form-group form-submit-group">
        {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
        {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-default']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@section('bottomscripts')
    <script>
        $(document).ready(function() {
            // functionality for the 'Add one' button
            var template = $('#hidden-template').html();
            $('#add_price').click(function() {

                if( $("#no_price").length) {
                    $("#no_price").remove();
                }
                var index = $('.js-new-supplement').length;
                $('#prices').append(template.replace(/\{index\}/g, index));
            });
        });
    </script>
@endsection