Informacje - repozytorium w celach rekrutacyjnych:

Wymagane php>=5.6.4
Wersja framework 5.4.*

1. Aby uruchomić należy skonfigurować bazę danych mysql w pliku .env

2. Uruchomić migracje

php artisan migrate

3. Uruchomić seeder'y - skorzystałem z fakera aby zapełnić dane

php artisan db:seed

Zadania
1. Zrobione.

2. Struktura bazy zgodna z przygotowanymi migracjami

3. Widok pod routingiem /product

4. Dodawanie produktów pod routingiem /product/create

5. Usuwanie produktów możliwe z listy produktów /product

Komentarz i plany na przyszłość - by to co zostało przygotowane było lepsze:
1. Zamiast przechowywania VAT'u jaku liczby, można pokusić się o relacjię do tabeli z VAT'ami

2. Dodanie walidatora z dannyvankooten/laravel-vat

3. W przyszłości można pokusić się o zastosowanie voyager

https://github.com/the-control-group/voyager

4. W przypadku walidacji VAT'u warto dać możliwość zmiany kierunku przeliczania z netto na brutto, z brutto na netto

5. Utworzenie typów "ceny" np. cena hurtowa, cena minimalna itp tak by użytkownik mógł skonfigurować sobie takie typy. Wiążę to za sobą konieczność utworzenia Modelu PriceType, w którym byłaby możliwość dodania typów

6. Przechowywanie cen archiwalnych.
