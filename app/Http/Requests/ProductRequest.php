<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pricesCount = count($this->get('prices'));

        $rules = [
            'name' => 'required|max:255',
            'description' => 'required',
        ];

        for ($i = 0; $i < $pricesCount; $i++) {
            $rules['prices.' . $i . '.netto'] = 'required|numeric';
            $rules['prices.' . $i . '.brutto'] = 'required|numeric';
            $rules['prices.' . $i . '.name'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'name.required' => 'Pole nazwa produktu jest wymagane',
            'description.required' => 'Pole opis produktu jest wymagane',
            'name.max' => 'Pole nazwa zawiera za dużo znaków',
        ];

//        $pricesCount = count($this->get('prices'));
////
////        for ($i = 0; $i < $pricesCount; $i++) {
////            $messages['prices.' . $i . '.netto' . 'required'] = 'required';
////            $messages['prices.' . $i . '.brutto' . 'required'] = 'required';
////            $messages['prices.' . $i . '.name' . 'required'] = 'required';
////        }

        return $messages;
    }
}
