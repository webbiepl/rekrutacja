<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Price;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->paginate(10);

        return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = Product::create($request->all());

        if($request->has('prices')) {
            $prices = $request->get('prices');

            foreach($prices as $price) {
                $product->prices()->create($price);
            }
        }

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $prices = $product->prices();

        return view('product.edit', ['product' => $product, 'prices' => $prices]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->update($request->all());

        if($request->has('prices')) {
            $pricesData = $request->get('prices');
            $prices = [];
            $pricesIds = [];

            foreach($pricesData as $price) {
                if (isset($price['id'])) {
                    Price::whereId($price['id'])
                        ->whereProductId($product->id)
                        ->update($price);

                    $pricesIds[] = $price['id'];

                } else {
                    $prices[] = new Price($price);
                }
            }

            if(count($pricesIds)) {
                Price::whereProductId($product->id)
                    ->whereNotIn('id', $pricesIds)
                    ->delete();
            }

            if(count($prices)) {
                $product->prices()
                    ->saveMany($prices);
            }
        }

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index');
    }
}
