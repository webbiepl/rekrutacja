<?php
use Illuminate\Database\Seeder;
use App\Product;
use App\Price;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $product = new Product();
            $product->name = $faker->company;
            $product->description = $faker->text(500);
            $product->save();

            $typesPrice = [
                1 => 'basePrice',
                2 => 'maximalPrice',
                3 => 'minimalPrice',
            ];

            for ($j = 0; $j++ < $faker->numberBetween(1,3); $j++) {
                $price = new Price();
                $netto = $faker->numberBetween(1000,5000);
                $price->netto = $netto;
                $price->brutto = 1.23 * $netto;
                $price->vat = 23;
                $price->name = $typesPrice[$j];
                $price->product_id = $product->id;
                $price->save();
            }
        }
    }
}
